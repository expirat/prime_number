package myproject.com;
import java.util.Scanner;
public class PrimeNumber {

	public static void main(String[] args) {
	
		int num=0;
		
		//citeste un numar pozitiv introdus de utilizator
		while(num<=0)
		   {
			System.out.println("Introdu un numar pozitiv:\n");
			Scanner numar=new Scanner(System.in);
			num=numar.nextInt();
		   }
			
	   //1 si 2 sunt exceptii, numere prime cunoscute
		if(num==1)
			System.out.println("Primul numar prim este: "+2);
		else if(num==2)
			System.out.println("Primul numar prim este: "+3);
		else
		{
			num++;
			boolean isnotPrime=true;
		//verifica numere pana gasete unul prim
			do 
			{
				for(int divisor = 2; divisor <= num / 2; divisor++)
					{
						if (num % divisor == 0) 
							{    
								isnotPrime = true;
								num++;
							}
						else isnotPrime=false;
					}
			}while(isnotPrime);
			System.out.println("Primul numar prim este: "+num);
		}
		
		
		
		
		
		
		
}
}